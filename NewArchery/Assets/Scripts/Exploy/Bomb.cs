﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {
    public float Damage;
    public GameObject explosionPrefab;
    private bool exploded = false;
    public static List<Bomb> AllBomb;
    public static object Mutex = new object();
    public static List<Zombie> AllZombie = new List<Zombie>();
    // Use this for initialization
    void Start () {
        lock (Mutex)
        {
            if (AllBomb == null)
                AllBomb = new List<Bomb>();
            AllBomb.Add(this);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void Explode()
    {
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);

        StartCoroutine(CreateExplosions(Vector3.forward*3));
        StartCoroutine(CreateExplosions(Vector3.right*3));
        StartCoroutine(CreateExplosions(Vector3.back*3));
        StartCoroutine(CreateExplosions(Vector3.left*3));

        StartCoroutine(DelayRemove());
        SubDammageZombie();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "arrow" )
        {
            Debug.Log("va cham 2");
            Invoke("Explode", 0.2f);
            
        }

    }
    void SubDammageZombie()
    {
        if (Zombie.AllZombie != null)
        {
            foreach (var zombie in Zombie.AllZombie)
            {
                if (Vector3.Distance(zombie.transform.position, this.transform.position) < 10f)
                {
                    zombie.GetComponent<Health>().Damage(this.Damage);
                }
            }
        }
    }
    IEnumerator DelayRemove()
    {
        yield return new WaitForSeconds(0.1f);
        if (this.GetComponent<Bomb>())
            this.GetComponent<Bomb>().RemoveFromAllBomb();
            this.gameObject.SetActive(false);
    }
    public void RemoveFromAllBomb()
    {
        lock (Mutex)
        {
            if (AllBomb == null)
            {

            }
            else
                AllBomb.Remove(this);
                
        }
    }
    private IEnumerator CreateExplosions(Vector3 direction)
    {
        for (int i = 1; i < 3; i++)
        { //The 3 here dictates how far the raycasts will check, in this case 3 tiles far
            RaycastHit hit; //Holds all information about what the raycast hits

            Physics.Raycast(transform.position + new Vector3(0, .5f, 0), direction, out hit, i); //Raycast in the specified direction at i distance, because of the layer mask it'll only hit blocks, not players or bombs
            if (!hit.collider)
            { // Free space, make a new explosion
                Instantiate(explosionPrefab, transform.position + (i * direction), explosionPrefab.transform.rotation);
            }
            else
            { //Hit a block, stop spawning in this direction
                break;
            }

            yield return new WaitForSeconds(.05f); //Wait 50 milliseconds before checking the next location
        }

    }
}
