﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class config {
    public float ArrowDragSpeed; // toc do keo cung
    public float ArrowRange;
    public float maxDistanceDummyAndPt_Dummy; // khoảng cách cực đại kéo cung.
    public float ZomebieMoveSpeed;
    public float flyScoreSpeed;
    public config()
    {
        ArrowDragSpeed = 0.1f;
        ArrowRange = 300;
        maxDistanceDummyAndPt_Dummy = 0.58f;
        ZomebieMoveSpeed = 5f;
        flyScoreSpeed = 3f;
    }
}
