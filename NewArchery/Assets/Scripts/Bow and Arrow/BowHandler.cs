﻿using UnityEngine;
using System.Collections;

public class BowHandler : MonoBehaviour
{
    public config _config;

    public GameObject dumMiddle; // day cung o giua
    public GameObject fabArrow; //tham chiếu đến mũi tên prefabs
    public GameObject gArrow; //tham chiếu đến mũi tên ban đầu

    public Transform pt_arrow; //điểm để sinh mũi tên mới

    public float ReloadDelay; //time trễ để load lại
    private TimerVar ReloadTimer; //time load bow.
    private bool _isReloading; //bow có trong quá trình nạp k.

    public float ArrowForce;
    public bool CanFire; //có được bắn cung hay không
    public bool isFire = true;

    public float distance;// khoảng cách giữa điểm cung ban đầu và sau khi kéo cung
    public GameObject pt_Dummiddle;// điểm đầu tiên của dây cung

    public GameObject bowString;
    public GameObject topString;
    public GameObject bottomString;
    Vector3 pos;
    public float arrowMinVelocity = 3f;
    public float arrowMaxVelocity = 30f;
    private float arrowVelocity = 30f;
    float velocity = 0f;
    void Start()
    {
        _config = new config();
        // pt_Dummiddle = new GameObject();
        ReloadTimer = new TimerVar(ReloadDelay, false);
        _isReloading = false;
        CanFire = true;
        // pt_Dummiddle.transform.position = dumMiddle.transform.position;
        if (gArrow == null)
        {
            ReloadArrow();
        }
    }

    // Update is called once per frame
    void Update()
    {
        KeyController();
    }
    public void KeyController()
    {
        Debug.DrawRay(transform.position, transform.forward * _config.ArrowRange, Color.red);

        if (CanFire)// nếu được bắn
        {
            //điều khiển dây cung
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //this.dumMiddle.transform.position -= dumMiddle.transform.right * _config.ArrowDragSpeed;
                velocity = Mathf.Abs(this.dumMiddle.transform.position.x - gArrow.transform.position.x);
                Debug.Log("velocity :" + velocity);
                bowString.SetActive(false);
                StartCoroutine(DrawBowString(topString.transform.position, gArrow.transform.position));
                StartCoroutine(DrawBowString(bottomString.transform.position, gArrow.transform.position));
                gArrow.transform.position -= gArrow.transform.forward * _config.ArrowDragSpeed;
                Debug.Log("Trigger down");

            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                bowString.SetActive(true);
                StartCoroutine(FireArrow());
                isFire = true;
                Debug.Log(" distance dummiddle and bow" + distance);
                Debug.Log("Trigger up");
            }
        }
        else
        {
            if (_isReloading)
            {
                if (ReloadTimer.TimerDone)
                {
                    _isReloading = false; //không load lại nữa, giảm các vòng lặp
                    ReloadTimer.Reset();
                    ReloadArrow();
                }
                else
                {
                    ReloadTimer.TimerUpdate();//load lại
                }
            }
        }
    }
    public Color c1 = Color.white;
    public Color c2 = Color.red;
    IEnumerator DrawBowString(Vector3 targetPos, Vector3 myPos)
    {

        //LineRenderer lineRend = gameObject.AddComponent<LineRenderer>();
        //lineRend.material = new Material(Shader.Find("Particles/Additive"));
        //lineRend.SetColors(c1, c2);
        //lineRend.SetPosition(0, myPos);
        //lineRend.SetPosition(1, targetPos);
        //lineRend.SetWidth(0.05f, 0.05f);



        GameObject lineRender = new GameObject();
        lineRender.AddComponent<LineRenderer>();
        lineRender.GetComponent<LineRenderer>().SetPosition(0, myPos);
        lineRender.GetComponent<LineRenderer>().SetPosition(1, targetPos);
        lineRender.GetComponent<LineRenderer>().SetWidth(0.05f, 0.05f);


        yield return new WaitForSeconds(0.2f);

        Destroy(lineRender);

    }
    //hàm sinh arrow mới
    public void ReloadArrow()
    {
        if (fabArrow != null)//kiểm tra tồn tại arrow prefabs
        {
            gArrow = (GameObject)Instantiate(fabArrow, pt_arrow.position, pt_arrow.rotation);
            gArrow.transform.SetParent(transform);

            //Now that the new arrow is in. We can fire again
            CanFire = true;
        }
        else
        {
            Debug.Log("Bow Error! You are trying to instantiate an object without a reference! Sent from: " + gameObject.name);
        }
    }

    //hàm dịch chuyển trạng thái của arrow
    public IEnumerator FireArrow()
    {
        if (gArrow != null)
        {

            //Get the rigidbody component of the arrow.
            Rigidbody rArrow = gArrow.GetComponent<Rigidbody>();

            if (rArrow != null)
            {
                gArrow.transform.SetParent(null);
                rArrow.isKinematic = false;

                ArrowHandler aHand = gArrow.GetComponent<ArrowHandler>();

                if (aHand != null)
                {
                    aHand.IsActive = true;
                }
               // 
                rArrow.AddForce(pt_arrow.forward * ArrowForce, ForceMode.Impulse);
                yield return new WaitForSeconds(0.03f);
                //rArrow.GetComponent<BoxCollider>().isTrigger = false;
                CanFire = false;

                //We've just fired the arrow, it's time to reload.
                _isReloading = true; //This will take care of everything to do with reloading.
            }
            else
            {
                Debug.Log("Bow Error! There is no rigidbody attached to the arrow object! Sent from: " + gArrow.name);
            }
        }
        else
        {
            //If it is null, then we need to have a little chat with ourselves.
            Debug.Log("Bow Error! The Arrow GameObject has not been set! Can't logically fire something that isn't there. Sent from: " + gameObject.name);
        }


    }


}
