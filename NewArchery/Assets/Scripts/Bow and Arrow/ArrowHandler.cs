﻿using UnityEngine;
using System.Collections;

public class ArrowHandler : MonoBehaviour {

    public float Lifetime; 
    public float DestroyDelay; 

    private TimerVar _lifeTimer; 
    private TimerVar _deathTimer; 
    private TimerVar _colTimer; 
    private bool _hitSomething;
    public bool IsActive;

    private Rigidbody _rig; 
    private Rigidbody _rigHuman;

    public GameObject pt_ShootArrow;
    public GameObject controlRight;
    private GameObject controlLeft;
    private GameObject bow;
    public GameObject pt_arrow;

    public AudioClip audioWithShoot;
    public AudioClip audioWithBow;
    public AudioSource audioSource;

    public GameObject fabParticleArrow;
    bool followBow = false;
    // Use this for initialization
    void Start () {
        audioSource.clip = audioWithShoot;
        audioSource.Play();
        if (pt_ShootArrow == null)
        {
            pt_ShootArrow = GameObject.Find("pt_shootArrow");
        }     
        if (controlLeft == null )
        {
            controlLeft = GameObject.Find("Controller (left)");
        }
       if(controlRight == null)
        {
            controlRight = GameObject.Find("Controller (right)");
        }
       if(pt_arrow == null)
        {
            pt_arrow = GameObject.Find("pt_point");
        }
        if (bow == null)
        {
            bow = GameObject.Find("Longbow (1)");
        }
        _rig = GetComponent<Rigidbody>(); 
        _rigHuman = GetComponent<Rigidbody>();
        //Set up our timers.
        _lifeTimer = new TimerVar(Lifetime, false);
        _deathTimer = new TimerVar(DestroyDelay, false);
	    
	}

	void Update () {

        if (!IsActive)
            return;

        if(_hitSomething)
        {
            if(_deathTimer.TimerDone)
            {
                Destroy(gameObject);
            }
            else
            {
                _deathTimer.TimerUpdate();
            }
        }
        else
        {
            if (_lifeTimer.TimerDone)
            {
                Destroy(gameObject);
            }
            else
            {
                _lifeTimer.TimerUpdate();
            }
        }	
	}

    void HitOjbect(GameObject obj)
    {
        SurfMaterial sF = obj.GetComponent<SurfMaterial>();
        if(sF != null)
        {
  
            switch(sF.SurfaceType)
            {
                case SurfMaterial.SurfType.Default:
                    break;
                case SurfMaterial.SurfType.Metal:
                    _hitSomething = true;
                    break;
                case SurfMaterial.SurfType.Wood:
                    _rig.isKinematic = true;
                    break;
                case SurfMaterial.SurfType.Human:
                    _rigHuman.isKinematic = true;
                    break;
                case SurfMaterial.SurfType.Rock:
                    Destroy(gameObject);
                    break;
            }
        }
        _hitSomething = true;
    }
    public void ArrowParticle()
    {
        if (fabParticleArrow != null)
        {
            GameObject particleObject = Instantiate(fabParticleArrow, transform.position, transform.rotation) as GameObject;
            particleObject.transform.SetParent(this.transform);
            particleObject.GetComponent<ParticleSystem>().Play();
            
        }
    }
    void FixedUpdate()
    {
        if (_hitSomething)
            return;
        if (_rig.velocity != Vector3.zero)
        {
            Quaternion aRot = Quaternion.LookRotation(_rig.velocity.normalized);
            _rig.rotation = Quaternion.Slerp(_rig.rotation, aRot, Time.deltaTime * 15);
        }

        if (_rigHuman.velocity != Vector3.zero)
        {
            Quaternion aRot = Quaternion.LookRotation(_rigHuman.velocity.normalized);
            _rigHuman.rotation = Quaternion.Slerp(_rigHuman.rotation, aRot, Time.deltaTime * 35);
        }
   }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "bow")
        {         
            this.transform.parent = pt_ShootArrow.transform;
            followBow = true;
            this.transform.position = this.pt_ShootArrow.transform.position;
            this.transform.rotation = pt_ShootArrow.transform.rotation;
        }
        if ( other.gameObject.tag == "zombie")
        {

            HitOjbect(other.gameObject);
            this.transform.SetParent(other.gameObject.transform);

        }
    }
    
    //khi an nut trigger
    public void OnBtnTrigger()
    {
        // tinh vector tu right den left
        Vector3 direction = controlLeft.transform.position - controlRight.transform.position;
        direction = direction.normalized; // huong di chuyen
        this.transform.forward = direction;
        // bow rotate follow controller right
        bow.transform.forward = direction;
    }

}



