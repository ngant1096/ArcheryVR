﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : MonoBehaviour {
    public config _config;
    public GameObject controllerLeft;
    public GameObject controllerRight;

    public LinearMap bowDrawLinearMapping;
    Animator animator;
    public static Bow Instance;
    public bool canArchery= false;
    // Use this for initialization
    private void Awake()
    {
        Instance = this;
    }
    void Start () {
        
            animator = GetComponent<Animator>();
        
        if (controllerLeft == null)
        {
            controllerLeft = GameObject.Find("Controller (left)");
        }
        if (controllerRight == null)
        {
            controllerRight = GameObject.Find("Controller (right)");
        }
        //this.bowDrawLinearMapping.value = 0.2f;


    }

    // Update is called once per frame
    void Update()
    {

    }
   
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "arrow")
        {
            canArchery = true;
            //this.transform.parent = controllerRight.transform;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "arrow")
        {
            //canArchery = false;
        }
    }
    IEnumerator DummyRoot()
    {

        yield return new WaitForSeconds(0.2f);
    }
}
