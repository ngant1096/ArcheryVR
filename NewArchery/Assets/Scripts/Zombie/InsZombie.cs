﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsZombie : MonoBehaviour {
    // 1 list time , thoi gian xuat hien cua zombie moi

    public GameObject fabZombieType1;
    public GameObject fabZombieType2;

    public float ReloadDelay; //time trễ để load lại
    public float ReloadTimer; //time load bow.
    private bool _isReloading = true; //bow có trong quá trình nạp k.

    MenuStart startMenuRefurent;

    public int indext = 0;
     int[] timeAppearWave1 = {3,9,10,12};
    int[] timeAppearWave2 = { 10,5,3,2,6,5,10,5, 12, 5, 4, 10 };
    int[] type = { 1, 1, 2, 1,1,1,2,2,1,1,1,2,2,1,1,1,0};
    int lenght;
    public int wave=1;
    void Start () {
        startMenuRefurent = MenuStart.Instance;
        Debug.Log("number "+ timeAppearWave1.Length);
        lenght = timeAppearWave1.Length;
	}
	
	// Update is called once per frame
	void Update () {
        if (startMenuRefurent.start == true)
        {
            if (!_isReloading)
            {
                if (ReloadTimer == 0 && indext < lenght)
                {

                    getTime(indext, wave);
                    _isReloading = true; //không load lại nữa, giảm các vòng lặp
                    ReloadZombie();
                    indext++;
                }
                else if (indext >= lenght && wave == 1)
                {
                    lenght = timeAppearWave2.Length;
                    wave = 2;
                    indext = 0;
                }
            }
            else
            {
                if (ReloadTimer > 0)
                {
                    ReloadTimer -= Time.deltaTime;
                }
                else if (ReloadTimer <= 0)
                {
                    ReloadTimer = 0;
                    _isReloading = false;
                }

            }
        }
    }
    public void getTime(int indexTime,int wave)
    {
        if(wave == 1 && timeAppearWave1.Length >0)
        {
            ReloadTimer = timeAppearWave1[indexTime];
        }else if(wave == 2 && timeAppearWave2.Length >0)
        {
            ReloadTimer = timeAppearWave2[indexTime];
        }
        
    }
    public void ReloadZombie()
    {

            if(fabZombieType1 != null && type[indext] == 1 && type[indext] != 0)
            { 
                fabZombieType1 = Instantiate(fabZombieType1, this.transform.position, this.fabZombieType1.transform.rotation);
            }else if(fabZombieType2 != null && type[indext] == 2 && type[indext] != 0)
            {
                fabZombieType2 = Instantiate(fabZombieType2, this.transform.position, this.fabZombieType2.transform.rotation);
            }
        
    }

}
