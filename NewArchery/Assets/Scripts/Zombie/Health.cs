﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public float MaxHP;
    float HP;
    // Use this for initialization
    void Start()
    {
        HP = MaxHP;
    }

    public bool Damage(float damage)
    {
        HP -= damage;
        if (HP <= 0)
        {
            StartCoroutine(DelayRemove());
            return true;
        }
        return false;
    }
    IEnumerator DelayRemove()
    {
        yield return new WaitForSeconds(0.1f);
        if (this.GetComponent<Zombie>())
            this.GetComponent<Zombie>().RemoveFromAllZombie();

        this.gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
