﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Zombie : MonoBehaviour {
    public static Zombie InstanceZombie; 

    public config _config;
    public float LifeTime;
    public float DestroyDelay;

    private bool _hitSomething; //va cham vao vat nao chua
    public bool CanMove; // con song hay da bi pha huy

    public static List<Zombie> AllZombie;
    public static object Mutex = new object();

    float maxLerp = -0.05f;
    float minLerp = 0.05f;
    static float t = 0.0f;
    // Use this for initialization
    public bool ZombieMoveThrough;
    public GameObject pt_zombie;
    public GameObject fabScore;

    //
    public GameObject target;
    public bool isNav;

    //
    public GameObject popPrefab;
    public GameObject lifetimeEndParticlePrefab;
    // 
    public static int score = 0;
    //
    public AudioClip audio;
    public GameObject fabBlood;
    public GameObject pt_Blood;

    bool init = true;
    bool isAttackGates = false;
    //
    MenuStart startMenuRefurent;
    GateHeath gateHeath;
    //state
    enum ZOMBIE_STATE
    {
        IDLE,
        MOVE,
        DIE
    }
    ZOMBIE_STATE curState;
    private void Awake()
    {
        InstanceZombie = this;
    }
    void Start () {
        _config = new config();
        CanMove = true;
        startMenuRefurent = MenuStart.Instance;
        gateHeath = GateHeath.GatesInstance;
        if(startMenuRefurent == null)
        {
            Debug.Log("err null start");
        }else
        {
            Debug.Log("bool" + startMenuRefurent.start);
        }

    }
	void InitZombie()//khoi tao cac thong so
    {
        if (startMenuRefurent.start == true)
        {
            lock (Mutex)
            {
                if (AllZombie == null)
                    AllZombie = new List<Zombie>();
                AllZombie.Add(this);
            }
            if (ZombieMoveThrough == true)
            {
                _MoveThrough();
            }
            if (isNav == true)
            {
                this.GetComponent<NavMeshAgent>().SetDestination(new Vector3(141, 2, -88));
                stateRun();
            }
            if (isNav == false && ZombieMoveThrough == false)
            {
                stateRun();
            }
            if (target == null && gateHeath.gateHeath > 0)
            {
                target = GameObject.Find("target");
            }

            if (isNav == false && ZombieMoveThrough == false && AllZombie.Count > 0)
            {
                _Move();
                //Invoke("newZombieForWave",10f);
            }
        }
    }

	// Update is called once per frame
	void Update () {
        
        if (startMenuRefurent.start == true && init==true)
        {
            InitZombie();
            init = false;
        }
        if (init == false && startMenuRefurent.start == true && AllZombie != null)
        {
            if (isNav == false && ZombieMoveThrough == false && AllZombie.Count > 0)
            {
                _Move();
                //Invoke("newZombieForWave",10f);
            }
            if ( gateHeath.gateHeath <= 0)
            {
                target = GameObject.Find("Coach (1)");
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "arrow")
        {
            CanMove = false;
            Debug.Log("arrow va cham zombie");
            SpawnParticles(lifetimeEndParticlePrefab);
            score += 10;
            creatScore();
            target = null;
            stateDead();
            Invoke("ZombieDie", 2f);
            if(isAttackGates == true)
            {
                gateHeath.isDisCressHeath = false;
            }
            TextMesh textObject = GameObject.Find("score").GetComponent<TextMesh>();
            textObject.text = score.ToString();
        }
        if (other.gameObject.tag == "gates")
        {
            stateAttack();
            isAttackGates = true;
        }
       
    }
    //hàm di chuyển qua lại khi không có destination
    public void _MoveThrough()
    {
        this.GetComponent<Animator>().SetBool("idle0ToRun", false);
        this.GetComponent<Animator>().SetBool("look", true);
        //gioi han di chuyen tọa độ worl position
        transform.position = new Vector3(this.transform.position.x, this.transform.position.y, Mathf.Lerp(this.transform.position.z + minLerp, this.transform.position.z + maxLerp, t));
        t += 0.1f * Time.deltaTime;
        if (t > 1.0f)
        {
            float temp = maxLerp;
            maxLerp = minLerp;
            minLerp = temp;
            t = 0.0f;
        }
    }
    private void SpawnParticles(GameObject particlePrefab)
    {
        // Don't do this twice

        if (particlePrefab != null)
        {
            GameObject particleObject = Instantiate(particlePrefab, pt_Blood.transform.position, transform.rotation) as GameObject;
            particleObject.GetComponent<ParticleSystem>().Play();
            Destroy(particleObject, 2f);
        }
    }
    void creatScore() // hieu ung diem
    {
        GameObject score = Instantiate(fabScore,this.transform);
        //score.transform.SetParent(this.transform);
        score.transform.position += score.transform.up *_config.flyScoreSpeed* Time.deltaTime;
    }
    public void ZombieDie() // khi chet
    {
        RemoveFromAllZombie();
        Destroy(gameObject);
    }
    public void RemoveFromAllZombie()
    {
        lock (Mutex)
        {
            if (AllZombie == null)
            {

            }
            else
                AllZombie.Remove(this);
        }
    }
    void _Move()
    {
        if (target != null)
        {
            Vector3 direction = target.transform.position - this.transform.position;
            if (direction.magnitude <= 1)
            {
                this.transform.position = this.target.transform.position;
            }
            else
            {
                this.transform.position += direction.normalized * _config.ZomebieMoveSpeed * Time.deltaTime;
            }
            this.transform.LookAt(target.transform);
        }
    }
    public void stateRun()
    {
        this.GetComponent<Animator>().SetBool("idle0ToRun", true);
        this.GetComponent<Animator>().SetBool("idle0ToDeath", false);
        this.GetComponent<Animator>().SetBool("ideToAttack", false);
    }
    public void stateAttack()
    {
        this.GetComponent<Animator>().SetBool("idle0ToRun", false);
        this.GetComponent<Animator>().SetBool("idle0ToDeath", false);
        this.GetComponent<Animator>().SetBool("ideToAttack", true);
    }
    public void stateDead()
    {
        this.GetComponent<Animator>().SetBool("idle0ToRun", false);
        this.GetComponent<Animator>().SetBool("idle0ToDeath", true);
        this.GetComponent<Animator>().SetBool("ideToAttack", false);
    }
}
