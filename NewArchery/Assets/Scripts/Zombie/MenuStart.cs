﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuStart : MonoBehaviour {
    public bool start = false;
    public static MenuStart Instance;
    // Use this for initialization
    private void Awake()
    {
        Instance = this;
    }
    // Update is called once per frame
    void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "arrow")
        {
            start = true;
        }
    }
}
