﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class NavMeshAgentTest : MonoBehaviour {
    private config _config;
    public GameObject target;
    public bool isNav;
	// Use this for initialization
	void Start () {
        _config = new config();
        if (isNav == true)
        {
            this.GetComponent<NavMeshAgent>().SetDestination(new Vector3(141, 2, -88));
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (isNav == false)
        {
            _Move();
        }
    }
    private void OnCollisionEnter(Collision other)
    {

        if (other.collider.tag == "gates")
        {
            
        }
    }
    void _Move()
    {
        Vector3 direction = target.transform.position - this.transform.position;
        if (direction.magnitude <= 1)
        {
            this.transform.position = this.target.transform.position;
        }
        else
        {
            this.transform.position += direction.normalized * _config.ZomebieMoveSpeed * Time.deltaTime;
        }
        this.transform.LookAt(target.transform);
    }
}
