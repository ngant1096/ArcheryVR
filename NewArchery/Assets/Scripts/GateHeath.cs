﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GateHeath : MonoBehaviour {
    public static GateHeath GatesInstance;
    public int gateHeath;
    public GameObject gatesL;
    public GameObject gatesR;
    public bool isDisCressHeath = false;

    //public Text lblHeath;
    private void Awake()
    {
        GatesInstance = this;
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       
        if (isDisCressHeath == true)
        {
            gateHeath -= 1;
            if(gateHeath <=0)
            {
                gateHeath = 0;
            }
            TextMesh textObject = GameObject.Find("gates").GetComponent<TextMesh>();
            textObject.text = gateHeath.ToString();
        }
        if(gateHeath <=0)
        {
            gatesL.SetActive(false);
            gatesR.SetActive(false);
        }
	}
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "zombie")
        {
            isDisCressHeath = true;

        }
    }
}
