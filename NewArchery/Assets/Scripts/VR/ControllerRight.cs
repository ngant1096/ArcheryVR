﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerRight : MonoBehaviour {

    private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
    public bool gripButtonDown = false;
    public bool gripButtonUp = false;
    public bool gripButtonPressed = false;


    private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
    public bool triggerButtonDown = false;
    public bool triggerButtonUp = false;
    public bool triggerButtonPressed = false;

    private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
    private SteamVR_TrackedObject trackedObj;

    public GameObject arrowHandler;

    public config _config;

    public GameObject fabArrow; //tham chiếu đến mũi tên prefabs
    public GameObject gArrow; //tham chiếu đến mũi tên ban đầu

    public Transform pt_arrow; //điểm để sinh mũi tên mới
    public GameObject pt_ShootArrow;// điểm đặt mũi tên trên cung


    public float ReloadDelay; //time trễ để load lại
    private TimerVar ReloadTimer; //time load bow.
    private bool _isReloading; //bow có trong quá trình nạp k.

    public float ArrowForce;
    public bool CanFire; //có được bắn cung hay không
    public GameObject bow;
    Bow bowHandle;
    ControllerLeft controllerScripts;
    public GameObject controlLeft;
    public GameObject pt_bow;  

    public GameObject dumMiddle; // day cung o giua
    public GameObject bowString;
    public GameObject topString;
    public GameObject bottomString;

    public float arrowMinVelocity = 3f;
    public float arrowMaxVelocity = 30f;
    private float arrowVelocity = 30f;
    float velocity = 0f;
    bool isCallDrawBow = false;
    public Material mat;

    Vector3 direction;// hướng của cung
    // Use this for initialization

    private void Start()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
        _config = new config();

        ReloadTimer = new TimerVar(ReloadDelay, false);
        _isReloading = false;
        CanFire = true;

        bowHandle = Bow.Instance;

        if (gArrow == null)
        {
            ReloadArrow();
        }
        if(bow == null)
        {
            bow = GameObject.Find("Longbow (1)");
        }
        if (controlLeft == null)
        {
            controlLeft = GameObject.Find("Controller (left)");
        }
        if(controllerScripts == null)
        {
            controllerScripts = new ControllerLeft();
        }
    }
    // Update is called once per frame
    void Update()
    {

        if (controller != null)
        {
            if (CanFire)
            {
                if (gripButtonDown)
                {
                    if (controller.GetPressDown(gripButton) && arrowHandler != null)
                    {
                        arrowHandler.transform.parent = this.transform;
                        arrowHandler.GetComponent<Rigidbody>().useGravity = false;
                    }
                }
                if (gripButtonUp)
                {
                }
                // nhan nút trigger ra
                if (triggerButtonDown)
                {
                    if (controller.GetPressDown(triggerButton) && gArrow != null && bowHandle.canArchery == true)
                    {
                        bowString.SetActive(false);
                        isCallDrawBow = true;

                    }
                }

                // nha nút trigger
                if (triggerButtonUp)
                {
                    if (controller.GetPressUp(triggerButton) && gArrow != null && bowHandle.canArchery == true)
                    {
                        isCallDrawBow = false;
                        bowString.SetActive(true);
                        gArrow.transform.parent = null;
                        FireArrow();
                        bow.transform.rotation = pt_bow.transform.rotation;
                        Debug.Log("trigger right = true");
                    }
                }

            }
            else
            {
                if (_isReloading)
                {
                    if (ReloadTimer.TimerDone)
                    {
                        _isReloading = false; //không load lại nữa, giảm các vòng lặp
                        ReloadTimer.Reset();
                        ReloadArrow();
                    }
                    else
                    {
                        ReloadTimer.TimerUpdate();//load lại
                    }
                }
            }
            //noi dung khi nhan nut trigger
            if (isCallDrawBow == true && CanFire)
            {

                velocity = Mathf.Abs(this.dumMiddle.transform.position.x - gArrow.transform.position.x);
                Debug.Log("velocity :" + velocity);
                StartCoroutine(DrawBowString(topString.transform.position, gArrow.transform.position));
                StartCoroutine(DrawBowString(bottomString.transform.position, gArrow.transform.position));
                OnBtnTrigger();
                if (velocity < 0.5f)
                {
                    gArrow.transform.position = pt_arrow.transform.position;
                    StartCoroutine(ArrowReleaseHaptics());
                }
            }
        }


    }
    private void OnTriggerEnter(Collider other)
    {
        //arrowHandler = other.gameObject;
        if(other.gameObject.tag == "cLeft")
        {
            Debug.Log("clef t");
        }
    }
    IEnumerator DrawBowString(Vector3 targetPos, Vector3 myPos)
    {

        GameObject lineRender = new GameObject();
        lineRender.AddComponent<LineRenderer>();
        lineRender.GetComponent<LineRenderer>().SetPosition(0, myPos);
        lineRender.GetComponent<LineRenderer>().SetPosition(1, targetPos);
        lineRender.GetComponent<LineRenderer>().SetWidth(0.01f, 0.01f);
        lineRender.GetComponent<LineRenderer>().material = mat;
        lineRender.GetComponent<LineRenderer>().SetColors(Color.gray, Color.gray);


        yield return new WaitForSeconds(0f);

        Destroy(lineRender);

    }
    //khi an nut trigger
    public void OnBtnTrigger()
    {
        if (gArrow != null)
        {
            // tinh vector tu right den left
            direction = controlLeft.transform.position - this.transform.position;
            direction = direction.normalized; // huong di chuyen
            gArrow.transform.forward = direction;
            // bow rotate follow controller right
            bow.transform.forward = direction;
        }
    }
    //ham sinh mui ten moi
    public void ReloadArrow()
    {
        if (fabArrow != null)//kiểm tra tồn tại arrow prefabs
        {
            gArrow = (GameObject)Instantiate(fabArrow, this.pt_arrow.transform.position, this.pt_arrow.transform.rotation);
            gArrow.transform.SetParent(transform);
            bowHandle.canArchery = false;
            //Now that the new arrow is in. We can fire again
            CanFire = true;
        }
        else
        {
            Debug.Log("Bow Error! You are trying to instantiate an object without a reference! Sent from: " + gameObject.name);
        }
    }
    //hàm dịch chuyển trạng thái của arrow
    public void FireArrow()
    {
        if (gArrow != null)
        {
            bowString.SetActive(true);
            isCallDrawBow = false;
            //Get the rigidbody component of the arrow.
            Rigidbody rArrow = gArrow.GetComponent<Rigidbody>();
            
            if (rArrow != null)
            {
                gArrow.transform.SetParent(null);
                rArrow.isKinematic = false;

                ArrowHandler aHand = gArrow.GetComponent<ArrowHandler>();

                if (aHand != null)
                {
                    aHand.IsActive = true;
                    aHand.ArrowParticle();
                }
                rArrow.GetComponent<BoxCollider>().isTrigger = false;
                rArrow.AddForce(direction *velocity* ArrowForce, ForceMode.Impulse);
                
                controller.TriggerHapticPulse(500);
                CanFire = false;

                //We've just fired the arrow, it's time to reload.
                _isReloading = true; //This will take care of everything to do with reloading.
            }
            else
            {
                Debug.Log("Bow Error! There is no rigidbody attached to the arrow object! Sent from: " + gArrow.name);
            }
        }
        else
        {
            //If it is null, then we need to have a little chat with ourselves.
            Debug.Log("Bow Error! The Arrow GameObject has not been set! Can't logically fire something that isn't there. Sent from: " + gameObject.name);
        }


    }
    private IEnumerator ArrowReleaseHaptics()
    {
        yield return new WaitForSeconds(0.05f);

        controller.TriggerHapticPulse(800);
        yield return new WaitForSeconds(0.05f);
    }
}
