﻿//using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerLeft : MonoBehaviour {
     private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
     public bool gripButtonDown = false;
     public bool gripButtonUp = false;
     public bool gripButtonPressed = false;
 
 
     private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
     public bool triggerButtonDown = false;
     public bool triggerButtonUp = false;
     public bool triggerButtonPressed = false;

    public SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
    private SteamVR_TrackedObject trackedObj;

    public bool isFire;

    public static ControllerLeft Instance;
    public bool canArchery = false;
    // Use this for initialization
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }
    // Update is called once per frame
    void Update()
    {
      
        if(controller != null)
        {
            if (gripButtonDown)
            {
                if(controller.GetPressDown(gripButton) )
                {

                }
            }
            if (gripButtonUp)
            {
            }
            // nha nut trigger
            if (triggerButtonDown)
            {
                if(controller.GetPressDown(triggerButton) )
                {
                    isFire = false;
                    Debug.Log("is fire == false");
                }
            }
            //nhan nut trigger
            if (triggerButtonUp)
            {
                if (controller.GetPressUp(triggerButton))
                {
                    isFire = true;
                    Debug.Log("is fire == true");
                }
            }
        }


    }


    private void OnTriggerEnter(Collider other)
    {
    }
    private void OnTriggerExit(Collider other)
    {
      //  bowHandler = null;
    }
}
